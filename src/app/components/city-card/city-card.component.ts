import {Component, Input} from '@angular/core';
import {City} from '../../models/interfaces';
import {WeatherService} from '../../services/weather.service';

@Component({
  selector: 'app-city-card',
  templateUrl: './сity-card.component.html',
  styleUrls: ['./city-card.component.scss']
})
export class CityCardComponent {

  constructor(private weatherService: WeatherService,
  ) {
  }

  @Input() city: City;

  // removes the city from the user list and resets the component value
  public deleteCity() {
    if (confirm(`Are you sure you want to delete the city of ${this.city.name}?`)) {
      this.weatherService.deleteCity(this.city);
      this.city = undefined;
    } else {
      return;
    }
  }

}
