import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'snowPrecipitationTransformation'
})
export class SnowPrecipitationTransformationPipe implements PipeTransform {
  transform(snow: object): any {
    const snowfullValues: any[] = Object.entries(snow);
    console.log(snowfullValues);
    let snowfullValue: number;
    let snowTimeInterval = '';
    if (snowfullValues.length === 1) {
      snowfullValue = snowfullValues[0][1];
      snowTimeInterval = snowfullValues[0][0];
    } else {
      snowfullValue = snowfullValues[1][1];
      snowTimeInterval = snowfullValues[1][0];
    }
    return `${snowfullValue} mm of snowfull in ${snowTimeInterval}`;
  }
}
