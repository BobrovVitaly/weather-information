import {Pipe, PipeTransform} from '@angular/core';
import {ListOfCountries} from '../models/listOfCountries';

@Pipe({
  name: 'transformCountryName'
})
export class TransformCountryNamePipe implements PipeTransform {

  constructor(
    private listOfCountries: ListOfCountries
  ) {
  }

  transform(countryAbbreviation: string): string {
    return this.listOfCountries.countries.find(country => country.alpha2 === countryAbbreviation).name || countryAbbreviation;
  }
}
