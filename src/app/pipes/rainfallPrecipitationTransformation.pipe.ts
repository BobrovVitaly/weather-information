import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'rainfallPrecipitationTransformation'
})
export class RainfallPrecipitationTransformationPipe implements PipeTransform {
  transform(rain: object): string {
    const rainfullValues: any[] = Object.entries(rain);
    console.log(rainfullValues);
    let rainfullValue: number;
    let rainTimeInterval = '';
    if (rainfullValues.length === 1) {
      rainfullValue = rainfullValues[0][1];
      rainTimeInterval = rainfullValues[0][0];
    } else {
      rainfullValue = rainfullValues[1][1];
      rainTimeInterval = rainfullValues[1][0];
    }
    return `${rainfullValue} mm of rain in ${rainTimeInterval}`;
  }

}
