import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CityCardComponent} from './components/city-card/city-card.component';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import { ContactComponent} from './views/contact/contact.component';
import { WeatherPageComponent} from './views/weather/weather-page.component';
import {MainMenuComponent} from './components/main-menu/main-menu.component';
import {ApiData} from './models/api-data';
import {ListOfCountries} from './models/listOfCountries';
import {AngularFireModule} from '@angular/fire';
import {environment} from '../environments/environment';
import {TransformWindDirectionPipe} from './pipes/transformWindDirection.pipe';
import {TransformCountryNamePipe} from './pipes/transformCountryName.pipe';
import {RainfallPrecipitationTransformationPipe} from './pipes/rainfallPrecipitationTransformation.pipe';
import {SnowPrecipitationTransformationPipe} from './pipes/snowPrecipitationTransformation.pipe';

@NgModule({
  declarations: [
    AppComponent,
    CityCardComponent,
    ContactComponent,
    WeatherPageComponent,
    MainMenuComponent,
    TransformWindDirectionPipe,
    TransformCountryNamePipe,
    RainfallPrecipitationTransformationPipe,
    SnowPrecipitationTransformationPipe,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    AngularFireModule.initializeApp(environment.firebase),
  ],
  providers: [ApiData, ListOfCountries],
  bootstrap: [AppComponent]
})
export class AppModule { }
