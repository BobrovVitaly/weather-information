import {Injectable} from '@angular/core';
import {City} from '../models/interfaces';

@Injectable({
  providedIn: 'root'
})

export class WeatherService {

  private cities: City[] = [];

  public getCities(): City[] {
    return this.cities;
  }

  public addCities(city: City) {
    if (city) {
      if (this.isInListOfCities(city.id)) {
        alert('the city is already on your list!');
      } else {
        this.getCities().push(city);
      }
    }
  }

  public isInListOfCities(cityId: string): boolean {
    return this.cities.some(city =>
      cityId === city.id
    );
  }

  public deleteCity(cityForDelete: City) {
    const indexForDelete = this.cities.findIndex(city => city.id === cityForDelete.id);
    this.cities.splice(indexForDelete, 1);
  }
}
