import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {City} from '../models/interfaces';
import {ApiData} from '../models/api-data';

@Injectable({
  providedIn: 'root'
})

export class LoadWeatherService {

  constructor(private httpClient: HttpClient,
              private apiDate: ApiData,
  ) {
  }

  private readonly apiKey = this.apiDate.apiKey;

  // sends get request for information about current weather in the selected city
  public loadCurrentCityWeatherInformation(cityId: string): Observable<City> {
    const url = `https://api.openweathermap.org/data/2.5/weather?id=${cityId}&units=metric&appid=${this.apiKey}`;
    return this.httpClient.get<City>(url);
  }

  // request for a list of cities by name
  public loadAListOfCitiesByName(cityName: string): Observable<any> {
    const url = `https://api.openweathermap.org/data/2.5/find?q=${cityName}&appid=${this.apiKey}`;
    return this.httpClient.get(url);
  }

}
