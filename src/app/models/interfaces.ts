export interface City {
  readonly coord?: Coord;
  readonly weather?: Weather;
  readonly base?: string;
  readonly main?: Main;
  readonly visibility?: string;
  readonly wind?: Wind;
  readonly rain?: any;
  readonly snow?: any;
  readonly clouds?: Clouds;
  readonly dt?: string;
  readonly sys?: Sys;
  readonly timezone?: string;
  readonly id: string;
  readonly name: string;
  readonly cod?: string;
}

export interface Weather {
  id?: string;
  main?: string;
  description?: string;
  icon?: string;
}

export interface Coord {
  lon?: string;
  lat?: string;
}

export interface Main {
  temp?: string;
  feels_like?: string;
  pressure?: number;
  humidity?: string;
}

export interface Wind {
  speed?: string;
  deg?: number;
  gust?: string;
}

export interface Clouds {
  all?: string;
}

export interface Sys {
  type?: string;
  id?: string;
  country?: string;
}

export interface Country {
  name: string;
  alpha2: string;
}
