import {Country} from './interfaces';

export class ListOfCountries {
  public readonly countries: Country[] =
    [
      {
        name: 'Abkhazia',
        alpha2: 'AB'
      },
      {
        name: 'Australia',
        alpha2: 'AU'
      },
      {
        name: 'Austria',
        alpha2: 'AT'
      },
      {
        name: 'Azerbaijan',
        alpha2: 'AZ'
      },
      {
        name: 'Albania',
        alpha2: 'AL'
      },
      {
        name: 'Algeria',
        alpha2: 'DZ'
      },
      {
        name: 'American Samoa',
        alpha2: 'AS'
      },
      {
        name: 'Anguilla',
        alpha2: 'AI'
      },
      {
        name: 'Angola',
        alpha2: 'AO'
      },
      {
        name: 'Andorra',
        alpha2: 'AD'
      },
      {
        name: 'Antarctica',
        alpha2: 'AQ'
      },
      {
        name: 'Antigua and Barbuda',
        alpha2: 'AG'
      },
      {
        name: 'Argentina',
        alpha2: 'AR'
      },
      {
        name: 'Armenia',
        alpha2: 'AM'
      },
      {
        name: 'Aruba',
        alpha2: 'AW'
      },
      {
        name: 'Afghanistan',
        alpha2: 'AF'
      },
      {
        name: 'Bahamas',
        alpha2: 'BS'
      },
      {
        name: 'Bangladesh',
        alpha2: 'BD'
      },
      {
        name: 'Barbados',
        alpha2: 'BB'
      },
      {
        name: 'Bahrain',
        alpha2: 'BH'
      },
      {
        name: 'Belarus',
        alpha2: 'BY'
      },
      {
        name: 'Belize',
        alpha2: 'BZ'
      },
      {
        name: 'Belgium',
        alpha2: 'BE'
      },
      {
        name: 'Benin',
        alpha2: 'BJ'
      },
      {
        name: 'Bermuda',
        alpha2: 'BM'
      },
      {
        name: 'Bulgaria',
        alpha2: 'BG'
      },
      {
        name: 'Bolivia, plurinational state of',
        alpha2: 'BO'
      },
      {
        name: 'Bonaire, Sint Eustatius and Saba',
        alpha2: 'BQ'
      },
      {
        name: 'Bosnia and Herzegovina',
        alpha2: 'BA'
      },
      {
        name: 'Botswana',
        alpha2: 'BW'
      },
      {
        name: 'Brazil',
        alpha2: 'BR'
      },
      {
        name: 'British Indian Ocean Territory',
        alpha2: 'IO'
      },
      {
        name: 'Brunei Darussalam',
        alpha2: 'BN'
      },
      {
        name: 'Burkina Faso',
        alpha2: 'BF'
      },
      {
        name: 'Burundi',
        alpha2: 'BI'
      },
      {
        name: 'Bhutan',
        alpha2: 'BT'
      },
      {
        name: 'Vanuatu',
        alpha2: 'VU'
      },
      {
        name: 'Hungary',
        alpha2: 'HU'
      },
      {
        name: 'Venezuela',
        alpha2: 'VE'
      },
      {
        name: 'Virgin Islands, British',
        alpha2: 'VG'
      },
      {
        name: 'Virgin Islands, U.S.',
        alpha2: 'VI'
      },
      {
        name: 'Vietnam',
        alpha2: 'VN'
      },
      {
        name: 'Gabon',
        alpha2: 'GA'
      },
      {
        name: 'Haiti',
        alpha2: 'HT'
      },
      {
        name: 'Guyana',
        alpha2: 'GY'
      },
      {
        name: 'Gambia',
        alpha2: 'GM'
      },
      {
        name: 'Ghana',
        alpha2: 'GH'
      },
      {
        name: 'Guadeloupe',
        alpha2: 'GP'
      },
      {
        name: 'Guatemala',
        alpha2: 'GT'
      },
      {
        name: 'Guinea',
        alpha2: 'GN'
      },
      {
        name: 'Guinea-Bissau',
        alpha2: 'GW'
      },
      {
        name: 'Germany',
        alpha2: 'DE'
      },
      {
        name: 'Guernsey',
        alpha2: 'GG'
      },
      {
        name: 'Gibraltar',
        alpha2: 'GI'
      },
      {
        name: 'Honduras',
        alpha2: 'HN'
      },
      {
        name: 'Hong Kong',
        alpha2: 'HK'
      },
      {
        name: 'Grenada',
        alpha2: 'GD'
      },
      {
        name: 'Greenland',
        alpha2: 'GL'
      },
      {
        name: 'Greece',
        alpha2: 'GR'
      },
      {
        name: 'Georgia',
        alpha2: 'GE'
      },
      {
        name: 'Guam',
        alpha2: 'GU'
      },
      {
        name: 'Denmark',
        alpha2: 'DK'
      },
      {
        name: 'Jersey',
        alpha2: 'JE'
      },
      {
        name: 'Djibouti',
        alpha2: 'DJ'
      },
      {
        name: 'Dominica',
        alpha2: 'DM'
      },
      {
        name: 'Dominican Republic',
        alpha2: 'DO'
      },
      {
        name: 'Egypt',
        alpha2: 'EG'
      },
      {
        name: 'Zambia',
        alpha2: 'ZM'
      },
      {
        name: 'Western Sahara',
        alpha2: 'EH'
      },
      {
        name: 'Zimbabwe',
        alpha2: 'ZW'
      },
      {
        name: 'Israel',
        alpha2: 'IL'
      },
      {
        name: 'India',
        alpha2: 'IN'
      },
      {
        name: 'Indonesia',
        alpha2: 'ID'
      },
      {
        name: 'Jordan',
        alpha2: 'JO'
      },
      {
        name: 'Iraq',
        alpha2: 'IQ'
      },
      {
        name: 'Iran, Islamic Republic of',
        alpha2: 'IR'
      },
      {
        name: 'Ireland',
        alpha2: 'IE'
      },
      {
        name: 'Iceland',
        alpha2: 'IS'
      },
      {
        name: 'Spain',
        alpha2: 'ES'
      },
      {
        name: 'Italy',
        alpha2: 'IT'
      },
      {
        name: 'Yemen',
        alpha2: 'YE'
      },
      {
        name: 'Cape Verde',
        alpha2: 'CV'
      },
      {
        name: 'Kazakhstan',
        alpha2: 'KZ'
      },
      {
        name: 'Cambodia',
        alpha2: 'KH'
      },
      {
        name: 'Cameroon',
        alpha2: 'CM'
      },
      {
        name: 'Canada',
        alpha2: 'CA'
      },
      {
        name: 'Qatar',
        alpha2: 'QA'
      },
      {
        name: 'Kenya',
        alpha2: 'KE'
      },
      {
        name: 'Cyprus',
        alpha2: 'CY'
      },
      {
        name: 'Kyrgyzstan',
        alpha2: 'KG'
      },
      {
        name: 'Kiribati',
        alpha2: 'KI'
      },
      {
        name: 'China',
        alpha2: 'CN'
      },
      {
        name: 'Cocos (Keeling) Islands',
        alpha2: 'CC'
      },
      {
        name: 'Colombia',
        alpha2: 'CO'
      },
      {
        name: 'Comoros',
        alpha2: 'KM'
      },
      {
        name: 'Congo',
        alpha2: 'CG'
      },
      {
        name: 'Congo, Democratic Republic of the',
        alpha2: 'CD'
      },
      {
        name: 'Korea, Democratic People\'s republic of',
        alpha2: 'KP'
      },
      {
        name: 'Korea, Republic of',
        alpha2: 'KR'
      },
      {
        name: 'Costa Rica',
        alpha2: 'CR'
      },
      {
        name: 'Cote d\'Ivoire',
        alpha2: 'CI'
      },
      {
        name: 'Cuba',
        alpha2: 'CU'
      },
      {
        name: 'Kuwait',
        alpha2: 'KW'
      },
      {
        name: 'Curaçao',
        alpha2: 'CW'
      },
      {
        name: 'Lao People\'s Democratic Republic',
        alpha2: 'LA'
      },
      {
        name: 'Latvia',
        alpha2: 'LV'
      },
      {
        name: 'Lesotho',
        alpha2: 'LS'
      },
      {
        name: 'Lebanon',
        alpha2: 'LB'
      },
      {
        name: 'Libyan Arab Jamahiriya',
        alpha2: 'LY'
      },
      {
        name: 'Liberia',
        alpha2: 'LR'
      },
      {
        name: 'Liechtenstein',
        alpha2: 'LI'
      },
      {
        name: 'Lithuania',
        alpha2: 'LT'
      },
      {
        name: 'Luxembourg',
        alpha2: 'LU'
      },
      {
        name: 'Mauritius',
        alpha2: 'MU'
      },
      {
        name: 'Mauritania',
        alpha2: 'MR'
      },
      {
        name: 'Madagascar',
        alpha2: 'MG'
      },
      {
        name: 'Mayotte',
        alpha2: 'YT'
      },
      {
        name: 'Macao',
        alpha2: 'MO'
      },
      {
        name: 'Malawi',
        alpha2: 'MW'
      },
      {
        name: 'Malaysia',
        alpha2: 'MY'
      },
      {
        name: 'Mali',
        alpha2: 'ML'
      },
      {
        name: 'United States Minor Outlying Islands',
        alpha2: 'UM'
      },
      {
        name: 'Maldives',
        alpha2: 'MV'
      },
      {
        name: 'Malta',
        alpha2: 'MT'
      },
      {
        name: 'Morocco',
        alpha2: 'MA'
      },
      {
        name: 'Martinique',
        alpha2: 'MQ'
      },
      {
        name: 'Marshall Islands',
        alpha2: 'MH'
      },
      {
        name: 'Mexico',
        alpha2: 'MX'
      },
      {
        name: 'Micronesia, Federated States of',
        alpha2: 'FM'
      },
      {
        name: 'Mozambique',
        alpha2: 'MZ'
      },
      {
        name: 'Moldova',
        alpha2: 'MD'
      },
      {
        name: 'Monaco',
        alpha2: 'MC'
      },
      {
        name: 'Mongolia',
        alpha2: 'MN'
      },
      {
        name: 'Montserrat',
        alpha2: 'MS'
      },
      {
        name: 'Burma',
        alpha2: 'MM'
      },
      {
        name: 'Namibia',
        alpha2: 'NA'
      },
      {
        name: 'Nauru',
        alpha2: 'NR'
      },
      {
        name: 'Nepal',
        alpha2: 'NP'
      },
      {
        name: 'Niger',
        alpha2: 'NE'
      },
      {
        name: 'Nigeria',
        alpha2: 'NG'
      },
      {
        name: 'Netherlands',
        alpha2: 'NL'
      },
      {
        name: 'Nicaragua',
        alpha2: 'NI'
      },
      {
        name: 'Niue',
        alpha2: 'NU'
      },
      {
        name: 'New Zealand',
        alpha2: 'NZ'
      },
      {
        name: 'New Caledonia',
        alpha2: 'NC'
      },
      {
        name: 'Norway',
        alpha2: 'NO'
      },
      {
        name: 'United Arab Emirates',
        alpha2: 'AE'
      },
      {
        name: 'Oman',
        alpha2: 'OM'
      },
      {
        name: 'Bouvet Island',
        alpha2: 'BV'
      },
      {
        name: 'Isle of Man',
        alpha2: 'IM'
      },
      {
        name: 'Norfolk Island',
        alpha2: 'NF'
      },
      {
        name: 'Christmas Island',
        alpha2: 'CX'
      },
      {
        name: 'Heard Island and McDonald Islands',
        alpha2: 'HM'
      },
      {
        name: 'Cayman Islands',
        alpha2: 'KY'
      },
      {
        name: 'Cook Islands',
        alpha2: 'CK'
      },
      {
        name: 'Turks and Caicos Islands',
        alpha2: 'TC'
      },
      {
        name: 'Pakistan',
        alpha2: 'PK'
      },
      {
        name: 'Palau',
        alpha2: 'PW'
      },
      {
        name: 'Palestinian Territory, Occupied',
        alpha2: 'PS'
      },
      {
        name: 'Palestinian Territory, Occupied',
        alpha2: 'PS'
      },
      {
        name: 'Panama',
        alpha2: 'PA'
      },
      {
        name: 'Holy See (Vatican City State)',
        alpha2: 'VA'
      },
      {
        name: 'Papua New Guinea',
        alpha2: 'PG'
      },
      {
        name: 'Paraguay',
        alpha2: 'PY'
      },
      {
        name: 'Peru',
        alpha2: 'PE'
      },
      {
        name: 'Pitcairn',
        alpha2: 'PN'
      },
      {
        name: 'Poland',
        alpha2: 'PL'
      },
      {
        name: 'Portugal',
        alpha2: 'PT'
      },
      {
        name: 'Puerto Rico',
        alpha2: 'PR'
      },
      {
        name: 'Macedonia, The Former Yugoslav Republic Of',
        alpha2: 'MK'
      },
      {
        name: 'Reunion',
        alpha2: 'RE'
      },
      {
        name: 'Russian Federation',
        alpha2: 'RU'
      },
      {
        name: 'Rwanda',
        alpha2: 'RW'
      },
      {
        name: 'Romania',
        alpha2: 'RO'
      },
      {
        name: 'Samoa',
        alpha2: 'WS'
      },
      {
        name: 'San Marino',
        alpha2: 'SM'
      },
      {
        name: 'Sao Tome and Principe',
        alpha2: 'ST'
      },
      {
        name: 'Saudi Arabia',
        alpha2: 'SA'
      },
      {
        name: 'Swaziland',
        alpha2: 'SZ'
      },
      {
        name: 'Saint Helena, Ascension And Tristan Da Cunha',
        alpha2: 'SH'
      },
      {
        name: 'Northern Mariana Islands',
        alpha2: 'MP'
      },
      {
      name: 'Saint Barthélemy',
      alpha2: 'BL'
      },
      {
        name: 'Saint Martin (French Part)',
        alpha2: 'MF'
      },
      {
        name: 'Senegal',
        alpha2: 'SN'
      },
      {
        name: 'Saint Vincent and the Grenadines',
        alpha2: 'VC'
      },
      {
        name: 'Saint Kitts and Nevis',
        alpha2: 'KN'
      },
      {
        name: 'Saint Lucia',
        alpha2: 'LC'
      },
      {
        name: 'Saint Pierre and Miquelon',
        alpha2: 'PM'
      },
      {
        name: 'Serbia',
        alpha2: 'RS'
      },
      {
        name: 'Seychelles',
        alpha2: 'SC'
      },
      {
        name: 'Singapore',
        alpha2: 'SG'
      },
      {
        name: 'Sint Maarten',
        alpha2: 'SX'
      },
      {
        name: 'Syrian Arab Republic',
        alpha2: 'SY'
      },
      {
        name: 'Slovakia',
        alpha2: 'SK'
      },
      {
        name: 'Slovenia',
        alpha2: 'SI'
      },
      {
        name: 'United Kingdom',
        alpha2: 'GB'
      },
      {
        name: 'United States',
        alpha2: 'US'
      },
      {
        name: 'Solomon Islands',
        alpha2: 'SB'
      },
      {
        name: 'Somalia',
        alpha2: 'SO'
      },
      {
        name: 'Sudan',
        alpha2: 'SD'
      },
      {
        name: 'Suriname',
        alpha2: 'SR'
      },
      {
        name: 'Sierra Leone',
        alpha2: 'SL'
      },
      {
        name: 'Tajikistan',
        alpha2: 'TJ'
      },
      {
        name: 'Thailand',
        alpha2: 'TH'
      },
      {
        name: 'Taiwan, Province of China',
        alpha2: 'TW'
      },
      {
        name: 'Tanzania, United Republic Of',
        alpha2: 'TZ'
      },
      {
        name: 'Timor-Leste',
        alpha2: 'TL'
      },
      {
        name: 'Togo',
        alpha2: 'TG'
      },
      {
        name: 'Tokelau',
        alpha2: 'TK'
      },
      {
        name: 'Tonga',
        alpha2: 'TO'
      },
      {
        name: 'Trinidad and Tobago',
        alpha2: 'TT'
      },
      {
        name: 'Tuvalu',
        alpha2: 'TV'
      },
      {
        name: 'Tunisia',
        alpha2: 'TN'
      },
      {
        name: 'Turkmenistan',
        alpha2: 'TM'
      },
      {
        name: 'Turkey',
        alpha2: 'TR'
      },
      {
        name: 'Uganda',
        alpha2: 'UG'
      },
      {
        name: 'Uzbekistan',
        alpha2: 'UZ'
      },
      {
        name: 'Ukraine',
        alpha2: 'UA'
      },
      {
        name: 'Wallis and Futuna',
        alpha2: 'WF'
      },
      {
        name: 'Uruguay',
        alpha2: 'UY'
      },
      {
        name: 'Faroe Islands',
        alpha2: 'FO'
      },
      {
        name: 'Fiji',
        alpha2: 'FJ'
      },
      {
        name: 'Philippines',
        alpha2: 'PH'
      },
      {
        name: 'Finland',
        alpha2: 'FI'
      },
      {
        name: 'Falkland Islands (Malvinas)',
        alpha2: 'FK'
      },
      {
        name: 'France',
        alpha2: 'FR'
      },
      {
        name: 'French Guiana',
        alpha2: 'GF'
      },
      {
        name: 'French Polynesia',
        alpha2: 'PF'
      },
      {
        name: 'French Southern Territories',
        alpha2: 'TF'
      },
      {
        name: 'Croatia',
        alpha2: 'HR'
      },
      {
        name: 'Central African Republic',
        alpha2: 'CF'
      },
      {
        name: 'Chad',
        alpha2: 'TD'
      },
      {
        name: 'Montenegro',
        alpha2: 'ME'
      },
      {
        name: 'Czech Republic',
        alpha2: 'CZ'
      },
      {
        name: 'Chile',
        alpha2: 'CL'
      },
      {
        name: 'Switzerland',
        alpha2: 'CH'
      },
      {
        name: 'Sweden',
        alpha2: 'SE'
      },
      {
        name: 'Svalbard and Jan Mayen',
        alpha2: 'SJ'
      },
      {
        name: 'Sri Lanka',
        alpha2: 'LK'
      },
      {
        name: 'Ecuador',
        alpha2: 'EC'
      },
      {
        name: 'Equatorial Guinea',
        alpha2: 'GQ'
      },
      {
        name: 'Åland Islands',
        alpha2: 'AX'
      },
      {
        name: 'El Salvador',
        alpha2: 'SV'
      },
      {
        name: 'Eritrea',
        alpha2: 'ER'
      },
      {
        name: 'Estonia',
        alpha2: 'EE'
      },
      {
        name: 'Ethiopia',
        alpha2: 'ET'
      },
      {
        name: 'South Africa',
        alpha2: 'ZA'
      },
      {
        name: 'South Georgia and the South Sandwich Islands',
        alpha2: 'GS'
      },
      {
        name: 'South Ossetia',
        alpha2: 'OS'
      },
      {
        name: 'South Sudan',
        alpha2: 'SS'
      },
      {
        name: 'Jamaica',
        alpha2: 'JM'
      },
      {
        name: 'Japan',
        alpha2: 'JP'
      },
    ];
}
