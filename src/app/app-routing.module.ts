import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {WeatherPageComponent} from './views/weather/weather-page.component';
import {ContactComponent} from './views/contact/contact.component';


const routes: Routes = [
  {
    path: 'weather', component: WeatherPageComponent,
  },
  {
    path: 'contact', component: ContactComponent,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
