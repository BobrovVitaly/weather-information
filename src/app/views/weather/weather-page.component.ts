import {Component, ElementRef, HostListener, OnInit, ViewChild} from '@angular/core';
import {City} from '../../models/interfaces';
import {WeatherService} from '../../services/weather.service';
import {LoadWeatherService} from '../../services/load-weather.service';

@Component({
  selector: 'app-weather-page',
  templateUrl: './weather-page.component.html',
  styleUrls: ['./weather-page.component.scss'],
})

export class WeatherPageComponent implements OnInit {

  constructor(private weatherService: WeatherService,
              private loadWeatherService: LoadWeatherService,
  ) {
  }

  citiesInUserList: City[]; // list of cities added by user
  citiesInSearchList: City[]; // list of cities found
  searchName = ''; // user enters to search for cities
  city: City;

  @ViewChild('foundCities', {static: false}) element: ElementRef;

  // if the user does not click on the selected item, do...
  @HostListener('click', ['$event']) onClick(event: MouseEvent) {
    if (!this.element.nativeElement.contains(event.target)) {
      this.clearSearchList(); // clear city search list
    }
  }

  ngOnInit(): void {
    // initialize the list of user cities (by default an empty array)
    this.citiesInUserList = this.weatherService.getCities();
  }

  // adds the selected city to the user list
  public addCity(cityId: string) {
    this.citiesInSearchList = [];
    const cityInformation = this.loadWeatherService.loadCurrentCityWeatherInformation(cityId);
    cityInformation.subscribe((value: any) => {
      this.weatherService.addCities(value);
      this.setCityCardValue(value);
      this.searchName = '';
    });
  }

  // checks if the city is in the user list
  public isInListOfCities(cityId: string): boolean {
    return this.weatherService.isInListOfCities(cityId);
  }

  // search city by user name
  public searchCities() {
    if (this.searchName.length >= 3) {
      this.loadWeatherService.loadAListOfCitiesByName(this.searchName)
        .subscribe(value => {
          this.citiesInSearchList = value.list;
        });
    }
  }

  // starts city search by pressing enter
  public keydownEnter(event: CustomEvent) {
    this.searchCities();
  }

  // sets value city-card
  public setCityCardValue(city: City) {
    this.city = city;
  }

  // clears the list of cities found
  public clearSearchList() {
    this.citiesInSearchList = undefined;
  }

}


