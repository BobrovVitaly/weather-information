// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyDWkFM0XRPU8tAi2p5a-AB00ECxbKpfPz4',
    authDomain: 'weather-information-bcab1.firebaseapp.com',
    databaseURL: 'https://weather-information-bcab1.firebaseio.com',
    projectId: 'weather-information-bcab1',
    storageBucket: 'weather-information-bcab1.appspot.com',
    messagingSenderId: '373347592730',
    appId: '1:373347592730:web:9448b308fc9e17b036bfa3',
    measurementId: 'G-B432JLMML1'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
